Algorithmia Drupal Module 7.x

This module provides integration with the Algorithmia API for advanced algorithm
usage on Drupal 7 sites.

Using this module will require an account with Algorithmia.
https://algorithmia.com

Configure your API key and enable algorithms for your site.
admin/config/system/algorithmia

The module is provided freely, use of the Algorithmia API through this
module  will require configuration of an API key. You may use the
free API key for the Recommendation system:

simrVXm+4zoYFbPtJwW783K8V6n1
